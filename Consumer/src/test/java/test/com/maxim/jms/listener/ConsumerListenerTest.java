/**
 * 
 */
package test.com.maxim.jms.listener;

import static org.junit.Assert.*;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.maxim.jms.listener.ConsumerListener;

/**
 * @author BobBaig
 *
 */
public class ConsumerListenerTest {

	private TextMessage message;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.maxim.jms.listener.ConsumerListener#onMessage(javax.jms.Message)}.
	 */
	@Test
	public void testOnMessage() {
		ConsumerListener listener = new ConsumerListener();
		listener.onMessage(message);
		assertNull(message);
	}
}
